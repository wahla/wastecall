#!/usr/bin/env python3

import argparse
from datetime import date
import csv
import os
import sys

try:
    import yaml
except ImportError:
    print('can not load pyyaml, please install it with "pip3 install pyyaml" ')
    print('https://pyyaml.org/wiki/PyYAMLDocumentation')
    exit(1)

try:
    import requests
except ImportError:
    print('can not load requests, please install it with "pip3 install requests"')
    exit(1)



class WasteCallConfig:
        def __init__(self,args):
            self.configfile = args.config_file
            with open(self.configfile, 'r') as stream:
                try:
                    self.configdata = yaml.safe_load(stream)

                except yaml.YAMLError as exc:
                    print(exc)
                    raise(exc)

def add_years(d, years):
    """Return a date that's `years` years after the date (or datetime)
    object `d`. Return the same calendar date (month and day) in the
    destination year, if it exists, otherwise use the following day
    (thus changing February 29 to March 1).

    """
    try:
        return d.replace(year = d.year + years)
    except ValueError:
        return d + (date(d.year + years, 1, 1) - date(d.year, 1, 1))

def query_csv(date_query):

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'https://www.awb-gp.de',
        'DNT': '1',
        'Connection': 'keep-alive',
        'Referer': 'https://www.awb-gp.de/termine/abfuhrtermine/',
        'Upgrade-Insecure-Requests': '1',
    }

    cookies = {
        'PHPSESSID': 'bb8458c94b9b3a90c75955158403b17e',
    }

    params = (
        ('key', '365d791b58c7e39b20bb8f167bd33981'),
        ('modus', 'd6c5855a62cf32a4dadbc2831f0f295f'),
        ('waction', 'export_csv'),
    )
    
    id1 ="29aef69673789ca87dbfecda48b02d55"
    id2 ="d1f6f62958cb6c51500b73fc031cefd1"
    
    data = [
    (id1, id2),
    ('f_id_kommune', '907'),
    ('f_posts_json[]', 'a:2:{s:32:"'+id1+'";s:32:"'+id2+'";s:12:"f_id_kommune";s:3:"907";}'),
    ('f_posts_json[]', 'a:4:{s:32:"'+id1+'";s:32:"'+id2+'";s:12:"f_id_kommune";s:3:"907";s:12:"f_posts_json";a:0:{}s:12:"f_id_strasse";s:4:"3952";}'),
    ('f_id_strasse', '3952'),
    ('f_id_abfalltyp_0', '18'),
    ('f_id_abfalltyp_1', '25'),
    ('f_id_abfalltyp_2', '20'),
    ('f_id_abfalltyp_3', '17'),
    ('f_id_abfalltyp_4', '59'),
    ('f_id_abfalltyp_5', '60'),
    ('f_id_abfalltyp_6', '19'),
    ('f_abfallarten_index_max', '8'),
    ('f_abfallarten', '18,25,20,17,59,60,19'),
    ('f_zeitraum', date_query),
    ('f_export_als', '{\'action\':\'https://api.abfall.io/?key=365d791b58c7e39b20bb8f167bd33981&modus=d6c5855a62cf32a4dadbc2831f0f295f&waction=export_csv\',\'target\':\'\'}'),
    ]

    response = requests.post('https://api.abfall.io/', headers=headers, params=params, cookies=cookies, data=data)
    return response

def getTowns():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0',
        'Accept': '*/*',
        'Accept-Language': 'en-US,en;q=0.5',
        #'Referer': 'https://www.awb-gp.de/',
        #'Origin': 'https://www.awb-gp.de',
        'Connection': 'keep-alive',
        'Cache-Control': 'max-age=0',
        'TE': 'Trailers',
    }

    params = (
        ('key', '365d791b58c7e39b20bb8f167bd33981'),
        ('modus', 'd6c5855a62cf32a4dadbc2831f0f295f'),
        ('waction', 'init'),
    )

    httpresponse = requests.post('https://api.abfall.io/', headers=headers, params=params)
    if httpresponse.status_code==200:
        response = []
        for line in httpresponse.text.split('\n'):
            if (line.startswith('<option value=')): # find only lines that describe a town
                #now extract the index number of the town (<option value="900"... )
                number_start = line.find('"',0,len(line))+1
                number_end = line.find('"',number_start,len(line))
                index_number = int(line[number_start:number_end])
                if index_number != 0: #the "bitte auswählen" option has index 0, we don't want to process that
                    name_start = line.find('>',0,len(line))+1
                    name_end = line.find('<',name_start,len(line))
                    name = line[name_start:name_end]
                    response.append({index_number,name})


        return response
    return None

def main():
    argparser = argparse.ArgumentParser(
        description="checks for waste collect dates and send via matrix")
    argparser.add_argument(
        "-c", "--config", help="configuration file", dest="config_file", required=True)
    config = WasteCallConfig(argparser.parse_args())
    response = getTowns()
    print(response)
    sys.exit(0) 

    date_actual_str = str(date.today().year)+str(date.today().month).zfill(2)+str(date.today().day).zfill(2)
    date_end = add_years(date.today(),1)
    date_end_str = str(date_end.year)+str(date_end.month).zfill(2)+str(date_end.day).zfill(2)
    date_query =date_actual_str+"-"+date_end_str 
    date_query = "20210101-20211231"
    print("-- query date "+date_query)

    csv_data=""
    csv_delim = ';'
    if os.path.isfile("cache.csv"):
        with open("cache.csv",'r') as cachefile:
            csv_data = cachefile.read()
            print("-- using cache")
    else:
        response = query_csv(date_query)
        print ("-- got "+str(response.status_code)+" from server")
        if response.status_code==200:
            with open("cache.csv",'w') as cachefile:
                cachefile.writelines(response.text)
            csv_data = response.text
        else:
            print("-- server error "+response.text)
            exit(1)
    print(csv_data)

    waste_topic = []

    waste_dates = []
    
    for line_nr, line in enumerate(csv_data.splitlines(),start=1):
        if line_nr==1 :
            for item in line.split(csv_delim):
                waste_topic.append(item)

        else:
            for nr, nextdate in enumerate(line.split(csv_delim)):
                if not nextdate.strip() =="":
                    waste_dates.append({nextdate,waste_topic[nr]})
            print( line)

    print (waste_topic)
    
    for d in waste_dates:
        print(d)


if __name__ == "__main__":
    # execute only if run as a script
    main()
